<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('includes.head')

</head>
<body>
    <aside class="main-menu">
        @include('includes.menu')
    </aside>
    <div id="fullpage">
        <section class="section home" data-anchor="home">
            <div class="overlay">
                <div class="main-content">
                    <h1>Ricardo den Dulk</h1>
                    <h3>Junior AllRound Developer</h3>
                    <small>Some text!!</small>
                </div>
            </div>
        </section>
        <section class="section about" data-anchor="about">
            <div class="intro">
                <div class="caption">
                    <h2>about</h2>
                    <small class="description">Some description goes here</small>
                </div>
                <a class="button choose">
                    <span>About Me</span>
                </a>
            </div>
            <div class="overlay video">
                <article class="video-article top left">
                    <div class="video-container initialized">
                        <div class="video-content">
                            <h3>Responsive Designs</h3>
                            <small>Some paragraph text</small>
                        </div>
                    </div>
                    <video data-autoplay muted loop class="video inside">
                        <source src="{{URL::asset('video/A-Night-Walk.mp4')}}" type="video/mp4">
                        <source src="{{URL::asset('video/A-Night-Walk.ogg')}}" type="video/ogg">
                    </video>
                </article>
                <article class="video-article top right">
                    <div class="video-container initialized">
                        <div class="video-content">
                            <h3>Clean Code</h3>
                            <small>Some paragraph text</small>
                        </div>
                    </div>
                    <video data-autoplay  muted loop class="video inside">
                        <source src="{{URL::asset('video/Six-Cent-Press.mp4')}}" type="video/mp4">
                    </video>
                </article>
                <article  class="video-article bottom left">
                    <div class="video-container initialized">
                        <div class="video-content">
                            <h3>Work</h3>
                            <small>Some paragraph text</small>
                        </div>
                    </div>
                    <video data-autoplay  muted loop class="video inside">
                        <source src="{{URL::asset('video/suitcase-one-shot-short-film.mp4')}}" type="video/mp4">
                    </video>
                </article>
                <article  class="video-article bottom right">
                    <div class="video-container initialized">
                        <div class="video-content">
                            <h3>Another Heading</h3>
                            <small>Some paragraph text</small>
                        </div>
                    </div>
                    <video data-autoplay  muted loop class="video inside">
                        <source src="{{URL::asset('video/A-Story.mp4')}}" type="video/mp4">
                    </video>
                </article>
            </div>
        </section>
        <section class="section portfolio" data-anchor="portfolio">
            <aside></aside>
            <div class="projects">
                <ul id="projects">
                    @foreach ($projects as $project)
                        <li>
                            @if ($project->checked === 1)
                                @include ('includes.project-top')
                            @else
                                @include ('includes.project-bottom')
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
            {{  $projects->links() }}
        </section>
        <section class="section progress" data-anchor="progress">
            Skills
        </section>
        <section class="section contact" data-anchor="contact">
            <div class="overlay" id="contact-section">
                <h3>Neem contact op?</h3>
                <a class="email" href="mailto:ricardodendulk1995@gmail.com">ricardodendulk1995@gmail.com</a>
                <ul>
                    <a href="#"><li><i class="fa fa-facebook" aria-hidden="true"></i></li></a>
                    <a href="#"><li><i class="fa fa-linkedin" aria-hidden="true"></i></li></a>
                    <a href="#"><li><i class="fa fa-slack" aria-hidden="true"></i></li></a>
                </ul>
                <a class="copyright"><i class="fa fa-copyright" aria-hidden="true"></i>  RICARDODENDULK <?php echo date("Y"); ?> ALL RIGHTS RESERVED</a>
            </div>
        </section>
        </div>
        <footer>
            @include('includes.footer')
        </footer>
    </body>
</html>


