<nav id="main-menu">
    <ul id="menu">
        <li data-menuanchor="home">
            <a href="#home"><span>Home</span></a>
        </li>
        <li data-menuanchor="about">
            <a href="#about"><span>About</span></a>
        </li>
        <li data-menuanchor="portfolio">
            <a href="#portfolio"><span>Projects</span></a>
        </li>
        <li data-menuanchor="progress">
            <a href="#progress"><span>Progress</span></a>
        </li>
        <li data-menuanchor="test">
            <a href="#contact"><span>Contact</span></a>
        </li>
    </ul>
</nav>

<nav id="secondary-menu">
    <ul class="menu-pages">
        <a id="button-pages">
            <li>
                <i class="fa fa-times" aria-hidden="true"></i>
            </li>
        </a>
    </ul>
</nav>