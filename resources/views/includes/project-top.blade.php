<div class="block image">
    <div class="triangle"></div>
</div>
<div class="block content">
    <div class="single-project-block">
        <h3>{{$project->title}}</h3>
        <p>{{$project->body}}</p>
        <a href="/project/{{ $project->id }}" class="portfolio-button"><span>More</span></a>
    </div>
</div>