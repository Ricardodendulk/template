<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('/css/admin.css') }}">
    <link rel="stylesheet" href="{{ asset('/storage/css/fonts.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/font-awesome.css') }}">


</head>
<body>
    <div id="app">
        <header class="admin-heading">
            @include ("includes.admin.header")
        </header>
        <aside>
            @include ("includes.admin.admin-navigation")
        </aside>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
</body>
</html>
