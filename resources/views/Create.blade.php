@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row create">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Create a project</h2></div>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <form method="POST" action="/admin/show" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title">Titel</label>
                        <input type="title" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Title" name="title">
                    </div>
                    <div class="form-group">
                        <label for="textarea">Commment</label>
                        <input type="textarea" class="form-control" id="comment" placeholder="Comment" name="body">
                    </div>
                    <div class="form-group">
                        <label for="upload">Upload</label>
                        <input type="file" class="form-control" id="upload" placeholder="upload" name="upload">
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="checked">
                            <!--<input type="text" class="form-check-input" name="checked" value="1">-->
                        </label>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                   @include ("layouts.errors")
                </form>
            </div>
        </div>
    </div>
@endsection
