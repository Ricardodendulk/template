@extends('layouts.app')

@section('content')
    <div class="left space"></div>

    <div class="container">
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        <table>
            <tr>
                <td>ID</td>
                <td>Title</td>
                <td>Content</td>
                <td>Boolean</td>
                <td></td>
            </tr>
            @foreach ($projects as $project)
                <tr>
                    <td>{{$project->id}}</td>
                    <td><a href="/projects/{{$project->id}}">{{$project->title}}</a></td>
                    <td>{{$project->body}}</td>
                    <td>{{$project->checked}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="{{ URL::to('admin/show/' . $project->id . '/edit') }}">
                                <button type="button" class="btn btn-warning">Edit</button>
                            </a>&nbsp;
                            <form action="{{url('admin/show', [$project->id])}}" method="POST">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-danger" value="Delete"/>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            {{$projects->links()}}
        </table>
    </div>



@endsection
