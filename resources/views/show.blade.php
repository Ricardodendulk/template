@extends('layouts.app')

@section('content')
    <div class="left space"></div>

    <div class="container">

            <h1>Showing Task {{ $projects->title }}</h1>

            <div class="jumbotron text-center">
                <p>
                    <strong>Task Title:</strong> {{ $projects->title }}<br>
                    <strong>Description:</strong> {{ $projects->body }}
                </p>
            </div>
    </div>



@endsection
