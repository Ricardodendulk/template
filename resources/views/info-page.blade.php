<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('includes.head')

</head>
<body>
<aside class="main-menu">
    @include('includes.menu-pages')
</aside>
<section>
    <div class="full single-pages">
        <div class="info block-pages">
            <div class="content block-pages">
                <span>date</span>
                <h3>{{$project->title}}</h3>
                <p>{{$project->body}}</p>
                <small>Auteur</small>
            </div>
        </div>
    </div>

</section>
<footer>
    @include('includes.footer')
</footer>
</body>
</html>


