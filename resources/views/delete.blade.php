@extends('layouts.app')

@section('content')
    <form action="{{url('/admin/index')}}" method="POST">
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="submit" class="btn btn-danger" value="Delete"/>
    </form>


@endsection
