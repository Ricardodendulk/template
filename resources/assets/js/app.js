
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./jquery.easings.min.js');
require('./jquery.fullpage.js');
//require('./scrolloverflow.min.js');



window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});


// --------- ABOUT --------- //

$( ".choose" ).click(function() {
    $( ".intro" ).fadeOut( "slow" );
    $( ".video-content" ).fadeIn("slow");
    $('.overlay.video').css('zIndex', '0');
});


//  -------- PROJECTS ------- //

$('#projects li').each(function(i, el) {
    // As a side note, this === el.
    if (i % 2 === 0) { $(this).attr("class", "even")}
    else { $(this).attr("class", "odd")}
});




$("#button-pages").click(function() {
    $("#main-menu").animate({
        width: "show"
    }, 1000);
    $("#secondary-menu").animate({
        width: "hide"
    }, 1000);
});



// ------ SHOW PAGE ------ //


$( ".user" ).click(function() {
    $( ".overlay" ).show();
    $( ".edit" ).show();
    $( ".delete" ).hide();
});
$( ".trash" ).click(function() {
    $( ".overlay" ).show();
    $( ".delete" ).show();
    $( ".edit" ).hide();
});


$( ".cancel" ).click(function() {
    $( ".overlay" ).hide();
    $( ".edit" ).hide();
    $(".delete").hide();
});


var deleteLog = false;

$(document).ready(function() {
    $('#fullpage').fullpage({
        anchors: ['home', 'about', 'portfolio', 'progress', 'contact'],
        menu: '#menu'
    });

});