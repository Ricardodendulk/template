<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Projects;
use Session;
use Intervention\Image\imageManagerStatic As Image;



class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        $projects = Projects::paginate(20);
        return view('index', compact('projects', $projects));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(request(), [
            "title" => "required",
            "body" => "required"
        ]);


        // Make new Project
        $project = new Projects;

        $project->title = $request->title;
        $project->body = $request->body;
        $project->checked = $request->has('checked');



        //Save image
//        if($request->hasFile('upload')) {
//            $project = $request->file('upload');
//            $filename = time() . '.' . $project->getClientOriginalExtension();
//            $location = public_path('images/' . $filename);
//            Image::make($project)->resize(800, 400)->save($location);
//
//            $project->image = $filename;
//        }

        if($request->hasFile('upload')) {
            $upload = $request->file('upload');
            $filename = time() . '.' . $upload->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($upload)->resize(800, 400)->save($location);

            $project->image = $filename;
        }



        $project->save();



        $project->save();

        // Return to a page
        return redirect('admin/index')->with('success', 'Project has been added');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Projects $projects)
    {
        return view('show',compact('projects',$projects));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit(Projects $projects)
    {
        return view('edit',compact('projects',$projects));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //Validate
        $request->validate([
            'title' => 'required|min:3',
            'body' => 'required',
        ]);


        Projects::find($id)->update($request->all());
        return redirect('admin/index')
            ->with('success','Article updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $projects = Projects::find($id);
        $projects->delete();

        $request->session()->flash('message', 'Successfully deleted the project!');
        return redirect('admin/index');

    }
}
