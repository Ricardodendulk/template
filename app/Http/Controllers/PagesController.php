<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projects;

class PagesController extends Controller
{
    public function index()
    {

        $projects = Projects::simplePaginate(3);



        return view('welcome', compact('projects'));

    }

    public function info()
    {
        return view('info-page');

    }

    public function show($id)
    {

        $project = Projects::find($id);
        return view('info-page', compact('project'));

    }



}
