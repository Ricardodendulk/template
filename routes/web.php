<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Projects;


Route::get('/', 'PagesController@index');
Route::get('/project', 'PagesController@info');
Route::get('/project/{projects}', 'PagesController@show');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/index', 'ProjectController@index');
Route::get('/admin/create', 'ProjectController@create');
Route::post('/admin/show', 'ProjectController@store');
Route::get('/admin/show/{projects}', 'ProjectController@show');
Route::get('/admin/show/{projects}/edit','ProjectController@edit');
Route::put('/admin/show/{projects}', 'ProjectController@update');
Route::delete('/admin/show/{projects}','ProjectController@destroy');





